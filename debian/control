Source: ledmon
Section: admin
Priority: optional
Maintainer: Hsieh-Tseng Shen <woodrow.shen@gmail.com>
Build-Depends: debhelper (>= 10), systemd, pkg-config, libsgutils2-dev, libudev-dev, libpci-dev
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/ledmon.git
Vcs-Browser: https://salsa.debian.org/debian/ledmon
Homepage: https://github.com/intel/ledmon

Package: ledmon
Architecture: linux-any
Depends: ${shlibs:Depends}, ${misc:Depends}, openipmi
Description: Enclosure LED Utilities
 ledmon and ledctl are userspace tools designed to control storage
 enclosure LEDs. The user must have root privileges to use these tools.
 .
 These tools use the SGPIO and SES-2 protocols to monitor and control LEDs.
 They been verified to work with Intel(R) storage controllers (i.e. the
 Intel(R) AHCI controller) and have not been tested with storage controllers of
 other vendors (especially SAS/SCSI controllers).
 .
 For backplane enclosures attached to ISCI controllers, support is limited to
 Intel(R) Intelligent Backplanes.
